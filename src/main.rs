#[macro_use] extern crate text_io;

use std::collections::BinaryHeap;
use std::cmp;
use std::cmp::Ordering;
use std::u32;

fn main() {
    let max_time: u32 = read!();
    let node_len: u32 = read!();
    let edge_len: u32 = read!();

    let mut nodes: Vec<Vec<Edge>> = (0..node_len).map(|_| Vec::new()).collect();

    for _ in 0..edge_len {
        let from: usize = read!();
        let to: usize = read!();
        let time: u32 = read!();
        let difficulty: u32 = read!();

        //We decrease it by one ince the actual id's are 1..n but arrays are 0..n-1
        nodes[from - 1].push(Edge::new(to - 1, time, difficulty));
    }

    //println!("{:#?}", &nodes);
    match shortest_path(&nodes, max_time) {
        Some(diff) => println!("{}", diff),
        None       => println!("UMULIGT"),
    }
}

fn shortest_path(nodes: &Vec<Vec<Edge>>, max_time: u32) -> Option<u32> {
    let mut distance: Vec<_> = (0..nodes.len()).map(|_| u32::MAX).collect();
    let mut priority_queue = BinaryHeap::new();
    let goal = nodes.len() - 1;

    distance[0] = 0;
    priority_queue.push(State { difficulty: 0, time: 0, position: 0 });

    while let Some(current) = priority_queue.pop() {
        // We have reached a level of difficulty where this is the easiest path we have found.
        // This means that we haven't found a path to the end, and we haven't found a viable path
        // including the previous *distance* written down. So we are sure we can use this one.
        distance[current.position] = current.time;

        // Alternatively we could have continued to find all shortest paths
        if current.position == goal { return Some(current.difficulty); }

        // Important as we may have already found a better way
        //if current.time > distance[position] { continue; }

        // For each node we can reach, see if we can find a way with
        // a lower cost going through this node
        for edge in &nodes[current.position] {
            let next = State {
                difficulty: cmp::max(current.difficulty, edge.difficulty),
                time: current.time + edge.time,
                position: edge.destination
            };

            //If the time is over the capacity, skip it
            if next.time > max_time {
                continue;
            }

            // If so, add it to the frontier and continue
            if next.time < distance[next.position] {
                priority_queue.push(next);
            }
        }
    }

    None
}

#[derive(Debug)]
struct Edge {
    difficulty: u32,
    time: u32,
    destination: usize,
}

impl Edge {
    fn new(destination: usize, time: u32, difficulty: u32) -> Self {
        Self {
            difficulty,
            time,
            destination,
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    difficulty: u32,
    time: u32,
    position: usize,
}

// The priority queue depends on `Ord`.
// Explicitly implement the trait so the queue becomes a min-heap
// instead of a max-heap.
impl Ord for State {
    fn cmp(&self, other: &State) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        other.difficulty.cmp(&self.difficulty)
            .then_with(|| other.time.cmp(&self.time))
            .then_with(|| self.position.cmp(&other.position))
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for State {
    fn partial_cmp(&self, other: &State) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
